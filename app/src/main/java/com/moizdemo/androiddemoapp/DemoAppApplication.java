package com.moizdemo.androiddemoapp;

import android.app.Application;

import com.cloudsherpas.googleplayservices.DemoAppController;

/**
 * Created by AMoizEsmail on 15/10/2015.
 */
public class DemoAppApplication extends Application {

    public DemoAppController controller;

    @Override
    public void onCreate() {
        super.onCreate();

        controller = new DemoAppController();
        controller.initializeService(this);

    }
}
