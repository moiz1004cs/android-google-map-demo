package com.moizdemo.androiddemoapp.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cloudsherpas.googleplayservices.listeners.OnMapListener;
import com.cloudsherpas.googleplayservices.models.Directions;
import com.cloudsherpas.googleplayservices.models.NearbyPlaces;
import com.cloudsherpas.googleplayservices.models.Place;
import com.cloudsherpas.googleplayservices.models.Step;
import com.moizdemo.androiddemoapp.Adapters.ListItemAdapter;
import com.moizdemo.androiddemoapp.DemoAppApplication;
import com.moizdemo.androiddemoapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by bofiaza on 21/10/2015.
 */
public class DirectionListFragment extends ListFragment implements View.OnClickListener, OnMapListener{

    EditText mSearchField;
    Button mSearchButton;

    private DemoAppApplication mDemoAppApplication;

    private ListItemAdapter mListItemAdapter;

    private List<Step> mStepList;

    private OnDirectionListFragmentListener directionListFragmentListener;

    public static Fragment onInstance() {
        return new DirectionListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.direction_list_fragment, container, false);

        mSearchField = (EditText) view.findViewById(R.id.edit_text_search_place);
        mSearchButton = (Button) view.findViewById(R.id.button_search);
        mSearchButton.setOnClickListener(this);

        mDemoAppApplication = (DemoAppApplication) getActivity().getApplicationContext();

        mStepList = new ArrayList<>();

        mListItemAdapter = new ListItemAdapter(mStepList, getActivity(),R.layout.view_list_item);
        setListAdapter(mListItemAdapter);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            directionListFragmentListener = (OnDirectionListFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                    " must implement OnDirectionListFragmentListener");
        }
    }

    @Override
    public void onClick(View view) {
        String destination = mSearchField.getText().toString();

        mDemoAppApplication.controller.getDirections("Philamlife Tower Makati", destination, this);
    }

    @Override
    public void onSearchResult(NearbyPlaces nearbyPlaces) {

    }

    @Override
    public void onGetDirection(Directions directions) {
        Log.d("Directions", directions.toString());

        mListItemAdapter.refreshListWithNewData(directions.routes.get(0).legs.get(0).steps);

        directionListFragmentListener.onDrawDirectionPolylines(directions);
    }

    public void searchNearbyPlace() {
        mDemoAppApplication.controller.searchPlaces("Philamlife Tower Makati", this);
    }

    @Override
    public void onGetDirectionFail(RetrofitError retrofitError) {

    }

    @Override
    public void onGetNearbyPlaces(NearbyPlaces nearbyPlaces) {

    }

    @Override
    public void onSearchAutoCompleteResult(List<Place> places) {

    }

    @Override
    public void onGetPlaceDetails(Place place) {

    }

    public interface OnDirectionListFragmentListener {
        void onDrawDirectionPolylines(Directions directions);
    }
}
