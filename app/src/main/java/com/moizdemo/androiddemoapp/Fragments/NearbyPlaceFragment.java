package com.moizdemo.androiddemoapp.Fragments;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloudsherpas.googleplayservices.listeners.OnMapListener;
import com.cloudsherpas.googleplayservices.models.Directions;
import com.cloudsherpas.googleplayservices.models.NearbyPlaces;
import com.cloudsherpas.googleplayservices.models.Place;
import com.moizdemo.androiddemoapp.Adapters.PlaceListAdapter;
import com.moizdemo.androiddemoapp.DemoAppApplication;
import com.moizdemo.androiddemoapp.R;

import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by bofiaza on 21/10/2015.
 */
public class NearbyPlaceFragment extends ListFragment implements OnMapListener{

    private DemoAppApplication demoAppApplication;

    private PlaceListAdapter placeListAdapter;

    public static Fragment onInstance() {
        return new NearbyPlaceFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_nearby_place, container, false);

        demoAppApplication = (DemoAppApplication) getActivity().getApplicationContext();

        placeListAdapter = new PlaceListAdapter(getActivity(), R.layout.view_set_destination_list_item);
        setListAdapter(placeListAdapter);


        demoAppApplication.controller.searchPlaces("14.5573,121.0220", this);

        return view;
    }


    @Override
    public void onSearchResult(NearbyPlaces nearbyPlaces) {
        placeListAdapter.setData(nearbyPlaces.results);
    }

    @Override
    public void onGetDirection(Directions directions) {

    }

    @Override
    public void onGetDirectionFail(RetrofitError retrofitError) {

    }

    @Override
    public void onGetNearbyPlaces(NearbyPlaces nearbyPlaces) {

    }

    @Override
    public void onSearchAutoCompleteResult(List<Place> places) {

    }

    @Override
    public void onGetPlaceDetails(Place place) {

    }
}
