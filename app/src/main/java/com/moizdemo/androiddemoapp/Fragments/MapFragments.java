package com.moizdemo.androiddemoapp.Fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.moizdemo.androiddemoapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AEsmail on 9/29/15.
 */
public class MapFragments extends Fragment implements OnMapReadyCallback{

    private GoogleMap mGoogleMap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng latLng = new LatLng(14.5573, 121.0220);
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Philamlife Tower"));

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));

        this.mGoogleMap = googleMap;

        setToolbarEnabled();

        hollowPolygons();

//        polygons();

    }

    private void setToolbarEnabled() {
        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
    }

    public void changeMapType(MapType mapType) {
        switch (mapType) {
            case MAP_TYPE_NONE:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
            case MAP_TYPE_NORMAL:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case MAP_TYPE_HYBRID:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case MAP_TYPE_SATELLITE:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case MAP_TYPE_TERRAIN:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
        }
    }

    public void polygons() {
        // Instantiates a new Polygon object and adds points to define a rectangle

        LatLng point = new LatLng(14.5573, 121.0220);

        //generate points on outter path
        ArrayList<LatLng> outter = new ArrayList<LatLng>();
        outter.add(new LatLng(point.latitude - 0.3, point.longitude - 0.3));
        outter.add(new LatLng(point.latitude - 0.3, point.longitude + 0.3));
        outter.add(new LatLng(point.latitude + 0.3, point.longitude + 0.3));
        outter.add(new LatLng(point.latitude + 0.3, point.longitude-0.3));

        PolygonOptions rectOptions = new PolygonOptions()
                .addAll(outter);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 11.0f));

        // Get back the mutable Polygon
        Polygon polygon = mGoogleMap.addPolygon(rectOptions);
    }

    public void hollowPolygons() {

        LatLng point = new LatLng(14.5573, 121.0220);

        //generate points on outter path
        ArrayList<LatLng> outter = new ArrayList<LatLng>();
        outter.add(new LatLng(point.latitude - 5, point.longitude - 5));
        outter.add(new LatLng(point.latitude - 5, point.longitude + 5));
        outter.add(new LatLng(point.latitude + 5, point.longitude + 5));
        outter.add(new LatLng(point.latitude + 5, point.longitude-5));

        //generate points on inner path
        ArrayList<LatLng> inner = new ArrayList<LatLng>();
        inner.add(new LatLng(point.latitude-4, point.longitude-4));
        inner.add(new LatLng(point.latitude-4, point.longitude+4));
        inner.add(new LatLng(point.latitude+4, point.longitude+4));
        inner.add(new LatLng(point.latitude + 4, point.longitude - 4));

        PolygonOptions polygonOptions =
                new PolygonOptions()
                        .addAll(outter)
                        .addHole(inner)
                        .fillColor(Color.BLUE)
                        .strokeWidth(1);

        mGoogleMap.addPolygon(polygonOptions);

    }

    public void addDirectionToMap(List<LatLng> latLngList) {
        Polyline mDirectionsPolyLine = mGoogleMap.addPolyline(new PolylineOptions()
                .addAll(latLngList)
                .color(Color.BLUE));
        Marker mDestinationMarker = mGoogleMap.addMarker(new MarkerOptions()
                .position(latLngList.get(latLngList.size() - 1)));
    }


    public enum MapType {
        MAP_TYPE_NORMAL,
        MAP_TYPE_NONE,
        MAP_TYPE_TERRAIN,
        MAP_TYPE_SATELLITE,
        MAP_TYPE_HYBRID
    }
}
