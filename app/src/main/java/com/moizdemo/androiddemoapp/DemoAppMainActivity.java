package com.moizdemo.androiddemoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.moizdemo.androiddemoapp.Activities.MainActivity;

/**
 * Created by AMoizEsmail on 15/10/2015.
 */
public class DemoAppMainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        DemoAppApplication demoAppApplication = (DemoAppApplication) getApplicationContext();
        demoAppApplication.controller.initializeService(this);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maain_demo);
    }
}
