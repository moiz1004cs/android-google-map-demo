package com.moizdemo.androiddemoapp.Adapters;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudsherpas.googleplayservices.models.Step;
import com.cloudsherpas.googleplayservices.services.MapService;
import com.moizdemo.androiddemoapp.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by bofiaza on 21/10/2015.
 */
public class ListItemAdapter extends BaseAdapter {

    //Required parameter is Step
    //Item list layout view is view_direction_list_item
    private List<Step> mStepList;
    private Activity mActivity;
    private int mResource;

    public ListItemAdapter(List<Step> mStepList, Activity mActivity, int mResource) {
        this.mStepList = mStepList;
        this.mActivity = mActivity;
        this.mResource = mResource;
    }


    @Override
    public int getCount() {
        if(mStepList != null) {
            return mStepList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return mStepList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder = null;

        if(convertView == null) {
            LayoutInflater inflater = mActivity.getLayoutInflater();
            convertView = inflater.inflate(mResource, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Step step = mStepList.get(position);
        String number = String.valueOf(position + 1);
        viewHolder.mDirectionStep.setText(
                number + ". " +
                        Html.fromHtml(step.htmlInstructions));

        double distance = 0;
        DecimalFormat decimalFormat = new DecimalFormat("%.1f");

        viewHolder.mDistance.setText(MapService.getDistanceKMFromString(step.distance.text));

        return convertView;
    }


    public class ViewHolder {


        TextView mDirectionStep;
        TextView mDistance;

        public ViewHolder(View view) {
            mDirectionStep = (TextView) view.findViewById(R.id.text_view_direction_title);
            mDistance = (TextView) view.findViewById(R.id.text_view_direction_distance);
        }

    }


    public void refreshListWithNewData(List<Step> steps) {
        this.mStepList = steps;
        this.notifyDataSetChanged();
    }
}
