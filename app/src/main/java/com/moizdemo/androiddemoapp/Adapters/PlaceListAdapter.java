package com.moizdemo.androiddemoapp.Adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cloudsherpas.googleplayservices.models.Place;
import com.moizdemo.androiddemoapp.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by bofiaza on 10/2/14.
 */
public class PlaceListAdapter extends BaseAdapter {

    private Activity mActivity;
    private int mLayoutId;
    private List<Place> mPlaceList;

    public PlaceListAdapter(Activity context, int resource) {
        mActivity = context;
        mLayoutId = resource;
        mPlaceList = new ArrayList<Place>();

    }

    @Override
    public int getCount() {
        if(mPlaceList != null) {
            return mPlaceList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return mPlaceList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if(convertView == null){
            LayoutInflater inflater =  mActivity.getLayoutInflater();
            convertView = inflater.inflate(mLayoutId, parent, false);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Place place = getPlace(position);
        if(place!=null)
            if(place.name!=null && !place.name.equals("") && place.vicinity!=null && !place.vicinity.equals("")){

                String placeName = place.name + ", "+place.vicinity;

                holder.placeName.setText(trimAddress(placeName));
            }else if(place.description != null && !place.description.equals("")) {
                holder.placeName.setText(trimAddress(place.description));
            }

        return convertView;
    }


    public static String trimAddress(String fullAddress) {

        String addressTrim = fullAddress.toString();

        if(!TextUtils.isEmpty(fullAddress)) {

            if(addressTrim.contains(", ON")) {
                addressTrim = addressTrim.substring(0, addressTrim.indexOf(", ON"));
                addressTrim += "";
            }

            if(addressTrim.contains(", Canada")) {
                addressTrim = addressTrim.substring(0, addressTrim.indexOf(", Canada"));
                addressTrim += "";
            }

        }

        return addressTrim;
    }

    public Place getPlace(int position){
        Place place = null;
        if(mPlaceList!=null && !mPlaceList.isEmpty()) {
            place = mPlaceList.get(position);
        }

        return place;
    }

    public void setData(List<Place> placeList) {

        if(placeList != null) {
            this.mPlaceList = placeList;

            this.notifyDataSetChanged();
        }
    }


    static class ViewHolder{
        TextView placeName;
        public ViewHolder(View view) {

            placeName = (TextView) view.findViewById(R.id.textview_place_name);
        }
    }
}
