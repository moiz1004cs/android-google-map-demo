package com.moizdemo.androiddemoapp.Activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;

import com.cloudsherpas.googleplayservices.models.Directions;
import com.cloudsherpas.googleplayservices.services.MapService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.moizdemo.androiddemoapp.Fragments.DirectionListFragment;
import com.moizdemo.androiddemoapp.Fragments.MapFragments;
import com.moizdemo.androiddemoapp.Fragments.NavigationDrawerFragment;
import com.moizdemo.androiddemoapp.Fragments.NearbyPlaceFragment;
import com.moizdemo.androiddemoapp.R;

public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, DirectionListFragment.OnDirectionListFragmentListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private android.app.FragmentManager fragmentManager;
    private android.app.FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        // initialize the map
//        mMapFragment = (MapFragment) getFragmentManager()
//                .findFragmentById(R.id.map);
//        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        MapFragments mapFragment;
        mapFragment = (MapFragments) fragmentManager.findFragmentById(R.id.map_fragment);

        StreetViewPanoramaFragment streetViewPanoramaFragment;
        streetViewPanoramaFragment = (StreetViewPanoramaFragment) fragmentManager.findFragmentById(R.id.streetviewpanorama);

        switch (position) {
            case 0:
                fragmentTransaction.show(mapFragment);
                fragmentTransaction.hide(streetViewPanoramaFragment);
                break;
            case 1:
                fragmentTransaction.hide(mapFragment);
                fragmentTransaction.show(streetViewPanoramaFragment);
                break;

            case 2:
                fragmentTransaction.hide(streetViewPanoramaFragment);
                fragmentTransaction.hide(mapFragment);
                fragmentTransaction.replace(R.id.container, DirectionListFragment.onInstance());
                break;
            case 3:
            case 4:
                fragmentTransaction.hide(streetViewPanoramaFragment);
                fragmentTransaction.hide(mapFragment);
                fragmentTransaction.replace(R.id.container, PlaceholderFragment.newInstance(position + 1));
                break;
            case 5:
                fragmentTransaction.hide(streetViewPanoramaFragment);
                fragmentTransaction.hide(mapFragment);
                fragmentTransaction.replace(R.id.container, NearbyPlaceFragment.onInstance());
                break;
            default:
                fragmentTransaction.show(mapFragment);
                fragmentTransaction.hide(streetViewPanoramaFragment);

        }


        fragmentTransaction.commit();

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.map);
                break;
            case 2:
                mTitle = getString(R.string.street_view);
                break;
            case 3:
                mTitle = getString(R.string.directions);
                break;
            case 4:
                mTitle = getString(R.string.places);
                break;
            case 5:
                mTitle = getString(R.string.auto_complete);
                break;
            case 6:
                mTitle = getString(R.string.nearby_place);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends android.app.Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    public void onNoneButtonClicked(View view) {


//        android.app.FragmentManager fragmentManager = getFragmentManager();
//        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragments mapFragment;
        mapFragment = (MapFragments) fragmentManager.findFragmentById(R.id.map_fragment);

        mapFragment.changeMapType(MapFragments.MapType.MAP_TYPE_NONE);
    }

    public void onNormalButtonClicked(View view) {
//        android.app.FragmentManager fragmentManager = getFragmentManager();
//        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragments mapFragment;
        mapFragment = (MapFragments) fragmentManager.findFragmentById(R.id.map_fragment);

        mapFragment.changeMapType(MapFragments.MapType.MAP_TYPE_NORMAL);
    }

    public void onTerainButtonClicked(View view) {

//        android.app.FragmentManager fragmentManager = getFragmentManager();
//        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragments mapFragment;
        mapFragment = (MapFragments) fragmentManager.findFragmentById(R.id.map_fragment);

        mapFragment.changeMapType(MapFragments.MapType.MAP_TYPE_TERRAIN);
    }

    public void onSatelliteButtonClicked(View view) {


//        android.app.FragmentManager fragmentManager = getFragmentManager();
//        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragments mapFragment;
        mapFragment = (MapFragments) fragmentManager.findFragmentById(R.id.map_fragment);

        mapFragment.changeMapType(MapFragments.MapType.MAP_TYPE_SATELLITE);
    }

    public void onHybridButtonClicked(View view) {
//        android.app.FragmentManager fragmentManager = getFragmentManager();
//        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragments mapFragment;
        mapFragment = (MapFragments) fragmentManager.findFragmentById(R.id.map_fragment);

        mapFragment.changeMapType(MapFragments.MapType.MAP_TYPE_HYBRID);
    }

    @Override
    public void onDrawDirectionPolylines(Directions directions) {
//        android.app.FragmentManager fragmentManager = getFragmentManager();
//        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragments mapFragment;
        mapFragment = (MapFragments) fragmentManager.findFragmentById(R.id.map_fragment);

        mapFragment.addDirectionToMap(MapService.decodePoly(directions.routes.get(0).overviewPolyline.points));
    }
}
