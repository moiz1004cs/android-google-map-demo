package com.cloudsherpas.googleplayservices;

import android.content.Context;

import com.cloudsherpas.googleplayservices.listeners.OnMapListener;
import com.cloudsherpas.googleplayservices.models.Directions;
import com.cloudsherpas.googleplayservices.models.NearbyPlaces;
import com.cloudsherpas.googleplayservices.models.PlaceDetails;
import com.cloudsherpas.googleplayservices.models.PlacePredictions;
import com.cloudsherpas.googleplayservices.services.IGoogleService;
import com.cloudsherpas.googleplayservices.services.GoogleService;
import com.cloudsherpas.googleplayservices.services.MapService;

import retrofit.Callback;
import retrofit.http.Query;

/**
 * Created by AMoizEsmail on 15/10/2015.
 */
public class DemoAppController {

    private MapService mapService;

    public DemoAppController() {
    }

    public void initializeService(Context context) {
        IGoogleService googleService = new GoogleService().getGoogleService();

        mapService = new MapService(googleService);
    }

    public void searchPlaces(String location, final OnMapListener onMapListener) {
       mapService.searchPlaces(location, onMapListener);
    }

    public void getDirections(final String originAddress, final String destinationAddress,
                              final OnMapListener onMapListener) {
        mapService.getDirections(originAddress, destinationAddress, onMapListener);
    }

    public void getNearbyPlaces(String location, String name, final OnMapListener onMapListener) {
        mapService.getNearbyPlaces(location, name, onMapListener);
    }

    public void getDetail(String placeId, final OnMapListener onMapListener) {
        mapService.getDetail(placeId, onMapListener);
    }

    public void searchPlaceAutoComplete(String input, String location, final OnMapListener onMapListener) {
        mapService.searchPlaceAutoComplete(input, location, onMapListener);
    }
}
