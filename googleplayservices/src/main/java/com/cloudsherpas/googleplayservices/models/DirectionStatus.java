package com.cloudsherpas.googleplayservices.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmatias on 10/2/14.
 */
public enum DirectionStatus {

    OK("OK"), // indicates the response contains a valid result.
    NOT_FOUND("NOT_FOUND"), //  indicates at least one of the locations specified in the requests's origin,
    // destination, or waypoints could not be geocoded.
    ZERO_RESULTS("ZERO_RESULTS"), // indicates no route could be found between the origin and destination.
    MAX_WAYPOINTS_EXCEEDED("MAX_WAYPOINTS_EXCEEDED"), // indicates that too many waypoints were provided in
    // the request. The maximum allowed waypoints is 8, plus the origin, and destination.
    // (Google Maps API for Business customers may contain requests with up to 23 waypoints.)
    INVALID_REQUEST("INVALID_REQUEST"), //indicates that the provided request was invalid. Common causes of this
    // status include an invalid parameter or parameter value.
    OVER_QUERY_LIMIT("OVER_QUERY_LIMIT"), //indicates the service has received too many requests from your
    // application within the allowed time period.
    REQUEST_DENIED("REQUEST_DENIED"), //indicates that the service denied use of the directions service by your application.
    UNKNOWN_ERROR("UNKNOWN_ERROR");


    private static final Map<String, DirectionStatus> lookup = new HashMap<String, DirectionStatus>();

    static {
        for (DirectionStatus d : DirectionStatus.values())
            lookup.put(d.value, d);
    }

    public String value;

    private DirectionStatus(String type) {
        this.value = type;
    }


    public static DirectionStatus factory(String value) {
        return getItemType(value);
    }


    public static DirectionStatus getItemType(String type) {
        return lookup.get(type);
    }
}
