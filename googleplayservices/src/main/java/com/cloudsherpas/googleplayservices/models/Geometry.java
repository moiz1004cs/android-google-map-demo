package com.cloudsherpas.googleplayservices.models;

/**
 * Created by kmatias on 10/2/14.
 */
public class Geometry {

    public LocationType location_type;

    public ViewPort viewport;

    public Coordinate location;
}
