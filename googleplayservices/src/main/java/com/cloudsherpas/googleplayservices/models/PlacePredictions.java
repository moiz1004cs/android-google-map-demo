package com.cloudsherpas.googleplayservices.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by bofiaza on 10/5/14.
 */
public class PlacePredictions {

    @SerializedName("predictions")
    @Expose
    public List<Place> places;
}
