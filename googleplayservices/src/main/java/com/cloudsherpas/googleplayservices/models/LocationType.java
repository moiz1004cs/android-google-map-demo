package com.cloudsherpas.googleplayservices.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmatias on 10/2/14.
 */
public enum LocationType {

    ROOFTOP("ROOFTOP"), // indicates that the returned result is a precise geocode
    // for which we have location information accurate down to street address precision.
    RANGE_INTERPOLATED("RANGE_INTERPOLATED"), // indicates that the returned result reflects an approximation
    // (usually on a road) interpolated between two precise points (such as intersections).
    // Interpolated results are generally returned when rooftop geocodes are unavailable for a street address.
    GEOMETRIC_CENTER("GEOMETRIC_CENTER"), // indicates that the returned result is the geometric center of a result such as a
    // polyline (for example, a street) or polygon (region).
    APPROXIMATE("APPROXIMATE"); // indicates that the returned result is approximate.


    // Reverse-lookup map for getting a type from a value
    private static final Map<String, LocationType> lookup = new HashMap<String, LocationType>();

    static {
        for (LocationType d : LocationType.values())
            lookup.put(d.value, d);
    }

    public String value;

    private LocationType(String type) {
        this.value = type;
    }

    public static LocationType factory(String value) {
        return getItemType(value);
    }


    public static LocationType getItemType(String type) {
        return lookup.get(type);
    }
}