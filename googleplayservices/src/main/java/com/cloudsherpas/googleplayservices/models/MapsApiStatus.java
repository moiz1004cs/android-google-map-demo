package com.cloudsherpas.googleplayservices.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmatias on 10/2/14.
 */
public enum MapsApiStatus {

    OK("OK"), // indicates that no errors occurred; the address was successfully parsed
    // and at least one geocode was returned.
    ZERO_RESULTS("ZERO_RESULTS"), // indicates that the geocode was successful but returned
    // no results. This may occur if the geocode was passed a non-existent
    // address or a latlng in a remote location.
    INVALID_REQUEST ("INVALID_REQUEST"), // generally indicates that the query (address or latlng) is missing.
    OVER_QUERY_LIMIT ("OVER_QUERY_LIMIT"), // indicates that you are over your quota.
    REQUEST_DENIED ("REQUEST_DENIED"); // indicates that your request was denied,
    // generally because of lack of a sensor parameter.


    private static final Map<String, MapsApiStatus> lookup = new HashMap<String, MapsApiStatus>();
    static {
        for (MapsApiStatus d : MapsApiStatus.values())
            lookup.put(d.value, d);
    }

    public String value;

    private MapsApiStatus(String type){
        this.value = type;
    }


    public static MapsApiStatus factory(String value) {
        return getItemType(value);
    }


    public static MapsApiStatus getItemType(String type){
        return lookup.get(type);
    }
}
