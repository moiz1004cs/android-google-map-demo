package com.cloudsherpas.googleplayservices.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kmatias on 9/23/14.
 */
public class Route {

    public String summary;

    public List<Leg> legs;

    @SerializedName("waypoint_order")
    @Expose
    public List<Integer> waypointOrder;

    @SerializedName("overview_polyline")
    @Expose
    public OverviewPolyLine overviewPolyline;

    public Object bounds;

    
    public String copyrights;

   
    public List<String> warnings;
}
