package com.cloudsherpas.googleplayservices.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kmatias on 10/2/14.
 */
public class Place {

    public Geometry geometry;

    public String name;

    public String icon;

    @SerializedName("place_id")
    @Expose
    public String placeId;

    public String reference;

    public String vicinity;

    public String address;

    public List<PlaceType> types;

    public boolean favorite;

    public double lat;

    public double lng;

    @SerializedName("closest_intersection")
    @Expose
    public String closestIntersection;

    @SerializedName("json_result")
    @Expose
    public String jsonResult;

    @SerializedName("details")
    @Expose
    public String details;

    @SerializedName("formatted_address")
    @Expose
    public String formattedAddress;

    public String description;

}
