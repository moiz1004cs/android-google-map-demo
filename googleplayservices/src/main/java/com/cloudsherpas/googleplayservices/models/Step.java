package com.cloudsherpas.googleplayservices.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kmatias on 9/23/14.
 */
public class Step {

    @SerializedName("html_instructions")
    @Expose
    public String htmlInstructions;

    public Duration duration;

    public Distance distance;

    public String maneuver;

    @SerializedName("end_location")
    @Expose
    public Coordinate endLocation;

    @SerializedName("start_location")
    @Expose
    public Coordinate startLocation;

    @SerializedName("travel_mode")
    public Object travelMode;

    public Object polyline;

}
