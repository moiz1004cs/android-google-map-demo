package com.cloudsherpas.googleplayservices.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kmatias on 10/2/14.
 */
public class NearbyPlaces {

    @SerializedName("html_attributions")
    @Expose
    public List<String> htmlAttributions;

    public List<Place> results;

    public MapsApiStatus status;

    @SerializedName("next_page_token")
    @Expose
    public String nextPageToken;

}
