package com.cloudsherpas.googleplayservices.models;

import com.cloudsherpas.googleplayservices.services.MapService;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kmatias on 10/2/14.
 */
public class Directions {

    public DirectionStatus status;

    public List<Route> routes;

    public List<LatLng> directionSteps;

    public List<LatLng> directionFromPoly;

    public void setRoutes(List<Route> routes) {
        this.routes = routes;

        if(!routes.isEmpty()) {
            setDirectionSteps(setUpDirectionSteps());
            setDirectionFromPoly(MapService.decodePoly(routes.get(0).overviewPolyline.points));
        }

    }
    private List<LatLng> setUpDirectionSteps() {
        List<LatLng> points = new ArrayList<LatLng>();

        // Add our very first point
        int i=0;
        List<Step> steps = routes.get(0).legs.get(0).steps;
        for(Step step : steps){
            if(i==0){
                points.add(new LatLng(Double.parseDouble(step.startLocation.lat), Double.parseDouble(step.startLocation.lng)));
                i++;
            }
            points.add(new LatLng(Double.parseDouble(step.endLocation.lat), Double.parseDouble(step.endLocation.lng)));
        }

        return points;
    }


    public List<LatLng> getDirectionSteps() {
        return directionSteps;
    }

    public void setDirectionSteps(List<LatLng> directionSteps) {
        this.directionSteps = directionSteps;
    }

    public List<LatLng> getDirectionFromPoly() {
        return directionFromPoly;
    }

    public void setDirectionFromPoly(List<LatLng> directionFromPoly) {
        this.directionFromPoly = directionFromPoly;
    }
}
