package com.cloudsherpas.googleplayservices.listeners;

import com.cloudsherpas.googleplayservices.models.Directions;
import com.cloudsherpas.googleplayservices.models.NearbyPlaces;
import com.cloudsherpas.googleplayservices.models.Place;

import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by kmatias on 10/2/14.
 */
public interface OnMapListener {
    public void onSearchResult(NearbyPlaces nearbyPlaces);
    public void onGetDirection(Directions directions);
    public void onGetDirectionFail(RetrofitError retrofitError);
    public void onGetNearbyPlaces(NearbyPlaces nearbyPlaces);
    public void onSearchAutoCompleteResult(List<Place> places);
    public void onGetPlaceDetails(Place place);
}
