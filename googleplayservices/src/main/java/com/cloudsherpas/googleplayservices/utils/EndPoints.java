package com.cloudsherpas.googleplayservices.utils;

/**
 * Created by AEsmail on 13/10/2015.
 */
public class EndPoints {

    //Google Service
    public static final String SEARCH_PLACE = "/place/nearbysearch/json";
    public static final String GET_DIRECTIONS ="/directions/json";
    public static final String GET_NEARBY_PLACES ="/place/nearbysearch/json";
    public static final String GET_DETAILS ="/place/details/json";
    public static final String SEARCH_PLACE_AUTOCOMPLETE = "/place/autocomplete/json";

}
