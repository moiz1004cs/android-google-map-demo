package com.cloudsherpas.googleplayservices.services;

import com.cloudsherpas.googleplayservices.models.Directions;
import com.cloudsherpas.googleplayservices.models.NearbyPlaces;
import com.cloudsherpas.googleplayservices.models.PlaceDetails;
import com.cloudsherpas.googleplayservices.models.PlacePredictions;
import com.cloudsherpas.googleplayservices.utils.EndPoints;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by AEsmail on 13/10/2015.
 */
public interface IGoogleService {

    @GET(EndPoints.SEARCH_PLACE)
    void searchPlaces(
            @Query("location") String location,
            @Query("sensor") boolean sensor,
            @Query("radius") int radius,
            @Query("key") String placesKey,
            Callback<NearbyPlaces> callback);

    @GET(EndPoints.GET_DIRECTIONS)
    void getDirections(
            @Query("origin") String originAddress,
            @Query("destination") String destinationAddress,
            @Query("sensor") boolean sensor,
            Callback<Directions> callback);

    @GET(EndPoints.GET_NEARBY_PLACES)
    void getNearbyPlacesRanked(
            @Query("location") String location,
            @Query("keyword") String name,
            @Query("sensor") boolean sensor,
            @Query("key") String placesKey,
            @Query("rankby") String rankby,
            Callback<NearbyPlaces> callback);

    @GET(EndPoints.GET_DETAILS)
    void getDetails(@Query("key") String key,
                    @Query("placeid") String id, Callback<PlaceDetails> callback);

    @GET(EndPoints.SEARCH_PLACE_AUTOCOMPLETE)
    void searchPlaceAutocomplete(@Query("key") String key,
                                 @Query("input") String input,
                                 @Query("components") String components,
                                 @Query("location") String location,
                                 @Query("radius") int radius,
                                 Callback<PlacePredictions> callback);
}
