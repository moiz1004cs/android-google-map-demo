package com.cloudsherpas.googleplayservices.services;

import com.cloudsherpas.googleplayservices.listeners.OnMapListener;
import com.cloudsherpas.googleplayservices.models.Directions;
import com.cloudsherpas.googleplayservices.models.NearbyPlaces;
import com.cloudsherpas.googleplayservices.models.PlaceDetails;
import com.cloudsherpas.googleplayservices.models.PlacePredictions;
import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by AEsmail on 13/10/2015.
 */
public class MapService  {

    public static final int MAP_RADIUS_NEAR = 100;
    public static final int CANADA_MAP_RADIUS_NEAR = 15000;
    public static String COMPONENTS = "country:ph";
    public static String RANK_BY_DEFAULT = "distance";
//    public static String GOOGLE_KEY = "AIzaSyD3ppuNQVqGX4bL0gUQhXiDFhCUwS28oHE";
    public static String GOOGLE_KEY = "AIzaSyDarQVtECuTbStru3Hj9z50SZHiLSx75c0";

    private IGoogleService googleService;


    public MapService(IGoogleService googleService) {
        this.googleService = googleService;
    }

    public void searchPlaces(String location, final OnMapListener onMapListener) {
        Callback<NearbyPlaces> callback = new Callback<NearbyPlaces>() {
            @Override
            public void success(NearbyPlaces nearbyPlaces, Response response) {
                if(onMapListener != null) {
                    onMapListener.onSearchResult(nearbyPlaces);
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }

        };

        googleService.searchPlaces(location, true, MAP_RADIUS_NEAR, GOOGLE_KEY, callback);

    }

    public void getDirections(final String originAddress, final String destinationAddress,
                              final OnMapListener onMapListener) {

        Callback<Directions> callback = new Callback<Directions>() {
            @Override
            public void success(Directions directions, Response response) {
                if(onMapListener != null) {
                    onMapListener.onGetDirection(directions);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        };

        googleService.getDirections(originAddress, destinationAddress, true, callback);
    }

    public void getNearbyPlaces(String location, String name, final OnMapListener onMapListener) {
        Callback<NearbyPlaces> callback = new Callback<NearbyPlaces>() {
            @Override
            public void success(NearbyPlaces nearbyPlaces, Response response) {
                if(onMapListener != null) {
                    onMapListener.onGetNearbyPlaces(nearbyPlaces);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        };

        googleService.getNearbyPlacesRanked(location, formatKeyword(name), true, GOOGLE_KEY, RANK_BY_DEFAULT, callback);

    }

    public void getDetail(String placeId, final OnMapListener onMapListener) {
        Callback<PlaceDetails> callback = new Callback<PlaceDetails>() {
            @Override
            public void success(PlaceDetails placeDetails, Response response) {
                if (onMapListener != null) {
                    onMapListener.onGetPlaceDetails(placeDetails.result);
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        };
        googleService.getDetails(GOOGLE_KEY, placeId, callback);
    }

    public void searchPlaceAutoComplete(String input, String location, final OnMapListener onMapListener) {


        Callback<PlacePredictions> callback = new Callback<PlacePredictions>() {
            @Override
            public void success(PlacePredictions predictions, Response response) {
                if(onMapListener != null && predictions != null) {
                    if(predictions.places != null) {
                        onMapListener.onSearchAutoCompleteResult(predictions.places);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        };
        googleService.searchPlaceAutocomplete(GOOGLE_KEY, input, COMPONENTS, location, CANADA_MAP_RADIUS_NEAR, callback);
    }

    private String formatKeyword(String keyword) {
        keyword.trim();
        keyword.replaceAll(" +", "+");

        return "\"" + keyword + "\"";
    }

    /**
     * Converts an encoded Polyline into a List of LatLng values.
     *
     * @param encoded
     * @return
     */
    public static List<LatLng> decodePoly(String encoded) {

        List<LatLng> polys = new ArrayList<LatLng>();

        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng(((double) lat / 1E5), ((double) lng / 1E5));
            polys.add(p);
        }

        return polys;
    }

    public static String getDistanceKMFromString(String distanceString) {
        String distance = "";

        double doubleDistance = 0;

        DecimalFormat df = new DecimalFormat("#.#");

        if (distanceString.contains("km")) {
            Pattern p = Pattern.compile("(\\d+(?:\\.\\d+))");
            Matcher matcher = p.matcher(distanceString);

            while (matcher.find()) {
                doubleDistance = Double.parseDouble(matcher.group(1));
            }

            distance = df.format(doubleDistance) + " km";
        } else {
            Pattern p = Pattern.compile("(\\d+)");
            Matcher matcher = p.matcher(distanceString);
            while (matcher.find()) {
                doubleDistance = Double.parseDouble(matcher.group(1)) / 1000; //TO km

            }

            distance = df.format(doubleDistance) + " km";
        }

        return distance;
    }

}
