package com.cloudsherpas.googleplayservices.services;

import retrofit.RestAdapter;

/**
 * Created by AEsmail on 13/10/2015.
 */
public class GoogleService {

    IGoogleService service;

    public GoogleService() {
    }

    public IGoogleService getGoogleService() {

        if (service != null) {
            return service;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://maps.googleapis.com/maps/api")
                .build();

        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);

        service = restAdapter.create(IGoogleService.class);

        return service;
    }
}
